# find recurring blocks of text

This is a script to _find recurring blocks of text_ in one or multiple files.

This might be helpful when looking for repeating patterns
(for examples in yaml- or toml-files)
that could be abstracted away.

## what is a recurring block of text

A block of text is hereby defined as multiple consequtive lines of text from a file.
A recurring block of text is therefore a block of text that occurs multiple times (in the exact same way) within one or multiple files.

## how can I influence the result?

The script tries to tell you only about the biggest blocks (number of lines) that come up most often (number of occurences).
You can tell it more specifically what you want via these flags:

- `--min_block_length` or `-l`
- `--max_block_length` or `-L`
- `--min_number_of_occurences` or `-o`

Find out more from running the script with `--help`.

# Example usage

Assume you have two yaml files (for a CI pipeline in this case):

```yml
# file_A.yml
typecheck:
  stage: test
  script:
    - warm up
    - install typechecker
    - run typecheck

unittests:
  stage: test
  script:
    - warm up
    - install dependencies
    - run unittests
```

```yml
# file_B.yml
ui_tests:
  stage: after_deployments
  script:
    - warm up
    - connect to ui
    - look at ui

integration_tests:
  stage: after_deployments
  script:
    - warm up
    - install dependencies
    - test groups_of_thing
```

Then running

```sh
find_recurring_blocks_of_text file_A.yml file_B.yml
```

will give you:

```
A block of length 3 with 2 references.
file_A.yml:2
file_A.yml:9
>   stage: test
>   script:
>     - warm up
A block of length 3 with 2 references.
file_A.yml:10
file_B.yml:10
>   script:
>     - warm up
>     - install dependencies
A block of length 3 with 2 references.
file_B.yml:2
file_B.yml:9
>   stage: after_deployments
>   script:
>     - warm up
```

You can specify whay you want more precisely:

```sh
find_recurring_blocks_of_text --min_number_of_occurences 3 file_A.yml file_B.yml
```

```
A block of length 2 with 4 references.
file_A.yml:3
file_A.yml:10
file_B.yml:3
file_B.yml:10
>   script:
>     - warm up
```

This seems to be a cancidate for a global `before-script`.
